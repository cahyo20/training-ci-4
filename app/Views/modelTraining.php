<?php foreach ($news as $news_item): ?>

<h3><?= $news_item['title'] ?></h3>

<div class="main">
    <?= esc($news_item['body']) ?>
</div>
<p><a href="/news/<?= esc($news_item['slug'], 'url') ?>">View article</a></p>

<?php endforeach; ?>