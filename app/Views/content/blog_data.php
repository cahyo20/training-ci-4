
    <div class="container">
        <div class="row">
          <div class="col-md-12" style="margin-top : 10px">
            <div id="message-error" class="alert alert-danger" role="alert">
              <h5>Error</h5>
              <span id="message-error-data">sample</span>
            </div>
          </div>
          <div class="col-lg-12">
            <div class="card">
                <h5 class="card-header">
                    <button onclick="addItem()" class="text-right btn btn-md btn-success">Add Item</button>
                </h5>

                <div class="card-body">
                <table id="example" class="table table-bordered" class="display" style="width:100%">
                <thead>
                    <tr>
                        <!-- <th>No</th> -->
                        <th>Title</th>
                        <th>Author</th>
                        <th>Desc</th>
                        <th>Aksi</th>

                    </tr>
                </thead>

            </table>
                </div>
                </div>
            </div>
        </div>
    </div>



<div class="modal fade bd-example-modal-lg" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">

    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="#" id="form-add-item" action="" method="post">
          <div class="row">
            <div class="col-md-12">
              <div id="message" class="alert" role="alert">
                <span id="message-data">sample</span>
              </div>
            </div>
            <div class="form-group col-md-4">
              <label for="">Title</label>
              <input class="form-control" placeholder="Blog Title...." type="text" name="blog_title" value="">
            </div>
            <div class="form-group col-md-4">
              <label for="">Author</label>
              <input class="form-control" placeholder="Author...." type="text" name="author" value="">
            </div>
            <div class="form-group col-md-4">
              <label for="">Description</label>
              <input class="form-control" placeholder="Description.." type="text" name="blog_description" value="">
            </div>
          </div>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" name="button" id="save" onclick="insertItem()" class="btn btn-sm btn-info">Simpan</button>
        <button type="button" name="button" id="reload" onclick="location.reload()" class="btn btn-sm btn-info">Refresh</button>
      </div>
    </div>

  </div>
</div>





    <script>
    $('#message-error').hide()
    $('#message').hide()
    $('#reload').hide()
        $(document).ready(function() {
            $('#example').DataTable({
                processing : true,
                serverSide : true,
                order : [],
                ajax : {
                    url : '<?=$url?>',
                    type : 'post',
                    error : function (err) {
                      $('#message-error').show()
                      $('#message-error-data').html(err.responseJSON.code +' : '+ err.responseJSON.message)
                    }
                },
                columnDefs: [{
                    targets: [],
                    orderable: false,
                }]
            });
        } );

        function addItem() {
          $('#addModal').modal('show')
        }

        function insertItem() {
          var data = $('#form-add-item').serializeArray()
          $.ajax({
            url: '/Desa/insert',
            type: 'POST',
            dataType: 'json',
            data: data
          })
          .done(function(response) {
            if (response.errCode == '00') {
              $('#message').addClass('alert-success')
              $('#reload').show()
              $('#save').hide()
            }else {
              $('#message').addClass('alert-warning')
            }
            $('#message').show()
            $('#message-data').html(response.message)

          })
          .fail(function(err) {
            $('#message').addClass('alert-danger')
            $('#message').show()
            $('#message-data').html(err.responseJSON.code +' : '+ err.responseJSON.message)
          })
        }


        function editItem(id) {
            console.log(id);
        }

        function deleteItem(id) {
            console.log(id);
        }
    </script>
