
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <div class="card">
                <h5 class="card-header">Featured</h5>
                <div class="card-body">
                <table id="example" class="table table-bordered" class="display" style="width:100%">
                <thead>
                    <tr>
                        <!-- <th>No</th> -->
                        <th>ID Desa</th>
                        <th>Nama Desa</th>
                        
                    </tr>
                </thead>
                
            </table>
                </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing : true,
                serverSide : true,
                order : [],
                ajax : {
                    url : '<?=$url?>',
                    type : 'post'
                },
                columnDefs: [{
                    targets: [],
                    orderable: false,
                }]
            });
        } );
    </script>
