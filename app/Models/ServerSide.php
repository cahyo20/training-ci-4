<?php

namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\HTTP\RequestInterface;

class ServerSide extends Model
{
    protected $request;
    protected $db;
    protected $dt;

    public function __construct(RequestInterface $request, $table)
    {
        parent::__construct();
        $this->table = $table;
        $this->db = db_connect();
        $this->request = $request;
        $this->dt = $this->db->table($table);

    }

    private function getDatatablesQuery($column_search, $column_order, $order)
    {
        $i = 0;
        foreach ($column_search as $item) {
            if ($this->request->getPost('search')['value']) {
                if ($i === 0) {
                    $this->dt->groupStart();
                    $this->dt->like($item, $this->request->getPost('search')['value']);
                } else {
                    $this->dt->orLike($item, $this->request->getPost('search')['value']);
                }
                if (count($column_search) - 1 == $i)
                    $this->dt->groupEnd();
            }else{

            }
            $i++;
        }

        if ($this->request->getPost('order')) {
            $this->dt->orderBy($column_order[$this->request->getPost('order')['0']['column']], $this->request->getPost('order')['0']['dir']);
        } else if (isset($order)) {
            $this->dt->orderBy(key($order), $order[key($order)]);
        }
    }

    public function getDatatables($column_search, $column_order, $order)
    {
        $this->getDatatablesQuery($column_search, $column_order, $order);
        if ($this->request->getPost('length') != -1)
            $this->dt->limit($this->request->getPost('length'), $this->request->getPost('start'));
        $query = $this->dt->get();
        return $query->getResult();
    }

    public function countFiltered($column_search, $column_order, $order)
    {
        $this->getDatatablesQuery($column_search, $column_order, $order);
        return $this->dt->countAllResults();
    }

    public function countAll()
    {
        $tbl_storage = $this->db->table($this->table);
        return $tbl_storage->countAllResults();
    }
}
