<?php

namespace App\Controllers;

// load model here....
use App\Models\ServerSide;
use Config\Services;


class ServerSideData extends BaseController
{
   
    public function getVillage()
    {
        $request = Services::request();
        // second param is table name..
        $datatable = new ServerSide($request, 'villages');


        // initialize param serverside ,.....
        $column_order   = ['id','name', 'district_id'];
        $column_search  = ['district_id', 'name'];
        $order          = ['name' => 'DESC'];

        if ($request->getMethod(true) === 'POST') {
            $lists = $datatable->getDatatables($column_search, $column_order, $order);
            $data = [];
            $no = $request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $row = [];
                // $row[] = $no;
                // start here to maninipulating data....
                $row[] = $list->district_id;
                $row[] = $list->name;
                $data[] = $row;
            }

            $output = [
                'draw' => $request->getPost('draw'),
                'recordsTotal' => $datatable->countAll(),
                'recordsFiltered' => $datatable->countFiltered($column_search, $column_order, $order),
                'data' => $data
            ];

            echo json_encode($output);
        }else{
            $output['message'] = 'Wrong method... !';
            echo json_encode($output);
        }
    }

    public function getBlog()
    {
        $request = Services::request();
        // second param is table name..
        $datatable = new ServerSide($request, 'blog');


        // initialize param serverside ,.....
        $column_order   = ['blog_id','blog_title', 'author', 'blog_description'];
        $column_search  = ['blog_title', 'author', 'blog_description'];
        $order          = ['blog_id' => 'DESC'];

        if ($request->getMethod(true) === 'POST') {
            $lists = $datatable->getDatatables($column_search, $column_order, $order);
            $data = [];
            $no = $request->getPost('start');

            foreach ($lists as $list) {
                $no++;
                $row = [];
                // $row[] = $no;

                // start here to maninipulating data....
                $row[] = $list->blog_title;
                $row[] = $list->author;
                $row[] = $list->blog_description;
                $row[] = '<div class="btn-group">
                            <button onclick="editItem('.$list->blog_id.')" class="btn btn-sm btn-primary">Edit</button>
                            <button onclick="deleteItem('.$list->blog_id.')" class="btn btn-sm btn-danger">Delete</button>
                        </div>';
                $data[] = $row;
            }

            $output = [
                'draw' => $request->getPost('draw'),
                'recordsTotal' => $datatable->countAll(),
                'recordsFiltered' => $datatable->countFiltered($column_search, $column_order, $order),
                'data' => $data
            ];

            echo json_encode($output);
        }else{
            $output['message'] = 'Wrong method... !';
            echo json_encode($output);
        }
    }
}
