<?php

namespace App\Controllers;

// load model here....
use App\Models\ServerSide;
use Config\Services;


class Desa extends BaseController
{
    public function __construct()
    {
        $this->db      = \Config\Database::connect();
    }
    public function index()
    {
        $set['url'] = '/ServerSideData/getVillage';
        $set['view'] = 'village_data';
        return view('main', $set);
    }
    public function blog()
    {
        $set['view'] = 'blog_data';
        $set['url'] = '/ServerSideData/getBlog';
        return view('main', $set);
    }
    public function insert()
    {
      $builder = $this->db->table('blog');
      $data = $this->request->getPost();
      try {
        if ($builder->insert($data)) {
          $res['message'] = 'success insert the item !!';
          $res['errCode'] = '00';
        } else {
          $res['message'] = 'error insert the item !!';
          $res['errCode'] = '99';
        }

      } catch (\Exception $e) {
        $res['message'] = 'error insert the item !!, exception.. : '. $e->getMessage();
        $res['errCode'] = '99';
      }

      echo json_encode($res);
    }
}
