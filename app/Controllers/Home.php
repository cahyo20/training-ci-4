<?php

namespace App\Controllers;

// load model here....
use App\Models\NewsModel;


class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}

	public function home()
	{
		$data['title'] = 'Ini Judul';
		return view('home',$data);
	}
	public function getNews()
	{
		$model = new NewsModel();
		$news = $model->getNews();

		print_r($news);

		// if (empty($data['news']))
		// {
		// 	throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the news item: '. $slug);
		// }
		//
		//
		// return view('modelTraining',$data);
	}
	public function create()
	{
		$model = new NewsModel();

		if ($this->request->getMethod() === 'post' && $this->validate([
				'title' => 'required|min_length[3]|max_length[255]',
				'body'  => 'required',
			]))
		{
			$model->save([
				'title' => $this->request->getPost('title'),
				'slug'  => url_title($this->request->getPost('title'), '-', TRUE),
				'body'  => $this->request->getPost('body'),
			]);

			echo view('success');

		}
		else
		{

			echo view('create');

		}
	}
	public function sample_api()
	{
		header("Content-type: application/json; charset=utf-8");
		$client = \Config\Services::curlrequest();

		try {
			//code...
			$response = $client->request('GET', 'https://reqres.in/api/users?page=2',  ['connect_timeout' => 1]);

			$data = json_decode($response->getBody());

			$res['errCode'] = '00';
			$res['message'] = $data;
		} catch (\Throwable $th) {
			//throw $th;
			$res['errCode'] = '99';
			$res['message'] = $th->getMessage();
		}

		echo json_encode($res, JSON_PRETTY_PRINT);


	}
}
